<?php

namespace Drupal\sitestudio_form_elements\Plugin\CustomElement;

use Drupal\cohesion_elements\CustomElementPluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Site Studio element to display the Form Field - Text Input.
 *
 * @CustomElement(
 *   id = "ss_form_field_text",
 *   label = @Translation("Text - Site Studio Form Field")
 * )
 */
class SiteStudioFormFieldText extends CustomElementPluginBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFields() {
    $fields = [];

    $fields['form_item_name'] = [
      'title' => 'Input Name',
      'type' => 'textfield',
      'placeholder' => $this->t('Machine name, will be used for sending in form. Example: first_name.'),
      'htmlClass' => 'col-xs-6',
      'required' => TRUE,
    ];

    $fields['form_item_required'] = [
      'title' => 'Input Required',
      'placeholder' => 'Specify whether the field .',
      'htmlClass' => 'col-xs-6',
      'type' => 'select',
      'nullOption' => FALSE,
      'options' => [
        'yes' => 'Yes',
        'no' => 'No',
      ],
      'defaultValue' => 'no',
    ];

    $fields['form_item_label'] = [
      'title' => 'Input Label',
      'type' => 'textfield',
      'placeholder' => $this->t('Label to display to end user. Leave empty to display without label.'),
      'htmlClass' => 'col-xs-12',
    ];

    $fields['form_item_placeholder'] = [
      'title' => 'Input Placeholder',
      'type' => 'textfield',
      'placeholder' => 'Help text to display to end user like this one is displayed to you.',
      'htmlClass' => 'col-xs-12',
    ];

    $fields['form_item_default_value'] = [
      'title' => 'Input Default Value',
      'type' => 'textfield',
      'placeholder' => 'Default value for the input.',
      'htmlClass' => 'col-xs-12',
    ];

    $fields['form_item_size'] = [
      'title' => 'Input Size',
      'type' => 'textfield',
      'placeholder' => 'Value to set size attribute of Input.',
      'htmlClass' => 'col-xs-12',
    ];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function render($element_settings, $element_markup, $element_class) {
    if (empty($element_settings['form_item_name'])) {
      return [];
    }

    $content['wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [$element_class],
      ],
    ];

    if (!empty($element_settings['form_item_label'])) {
      $content['wrapper'][$element_settings['form_item_label']] = [
        '#type' => 'html_tag',
        '#tag' => 'label',
        '#value' => $element_settings['form_item_label'],
        '#attributes' => [
          'class' => ['ss-form-input-label'],
          'for' => $element_settings['form_item_name'],
        ],
      ];
    }

    $input = [
      '#type' => 'html_tag',
      '#tag' => 'input',
      '#attributes' => [
        'type' => 'text',
        'class' => ['ss-form-input', 'ss-form-input-text-field'],
        'name' => $element_settings['form_item_name'],
      ],
    ];

    if (!empty($element_markup['classes'])) {
      $input['#attributes']['class'][] = $element_markup['classes'];
    }

    if (!empty($element_markup['id'])) {
      $input['#attributes']['id'][] = $element_markup['id'];
    }

    // Add the attributes from SS to Input.
    foreach ($element_markup['attributes'] ?? [] as $element_attributes) {
      if (empty($input['#attributes'][$element_attributes['attribute']])) {
        $input['#attributes'][$element_attributes['attribute']] = $element_attributes['value'];
      }
    }

    if (!empty($element_settings['form_item_default_value'])) {
      $input['#attributes']['value'] = $element_settings['form_item_default_value'];
    }

    if ($element_settings['form_item_required'] === 'yes') {
      $input['#attributes']['required'] = 'required';
    }

    if (!empty($element_settings['form_item_placeholder'])) {
      $input['#attributes']['placeholder'] = $element_settings['form_item_placeholder'];
    }

    if (!empty($element_settings['form_item_size'])) {
      $input['#attributes']['size'] = $element_settings['form_item_size'];
    }

    $content['wrapper'][$element_settings['form_item_name']] = $input;

    return $content;
  }

}
