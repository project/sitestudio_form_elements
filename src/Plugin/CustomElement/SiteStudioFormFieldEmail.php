<?php

namespace Drupal\sitestudio_form_elements\Plugin\CustomElement;

/**
 * Site Studio element to display the Form Field - Email Input.
 *
 * @CustomElement(
 *   id = "ss_form_field_email",
 *   label = @Translation("Email - Site Studio Form Field")
 * )
 */
class SiteStudioFormFieldEmail extends SiteStudioFormFieldText {

  /**
   * {@inheritdoc}
   */
  public function getFields() {
    $fields = parent::getFields();
    $fields['form_item_name']['placeholder'] = $this->t('Machine name, will be used for sending in form. Example: email.');
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function render($element_settings, $element_markup, $element_class) {
    $content = parent::render($element_settings, $element_markup, $element_class);

    if (empty($content)) {
      return $content;
    }

    $content['wrapper'][$element_settings['form_item_name']]['#attributes']['class'][1] = 'ss-form-input-email-field';
    $content['wrapper'][$element_settings['form_item_name']]['#attributes']['type'] = 'email';

    return $content;
  }

}
