<?php

namespace Drupal\sitestudio_form_elements\Plugin\CustomElement;

/**
 * Site Studio element to display the Form Field - Number Input.
 *
 * @CustomElement(
 *   id = "ss_form_field_number",
 *   label = @Translation("Number - Site Studio Form Field")
 * )
 */
class SiteStudioFormFieldNumber extends SiteStudioFormFieldText {

  /**
   * {@inheritdoc}
   */
  public function getFields() {
    $fields = parent::getFields();
    $fields['form_item_name']['placeholder'] = $this->t('Machine name, will be used for sending in form. Example: quantity.');

    $fields['form_item_min'] = [
      'title' => 'Minimum Value',
      'type' => 'textfield',
      'placeholder' => 'Minimum value allowed for the input.',
      'htmlClass' => 'col-xs-4',
    ];

    $fields['form_item_max'] = [
      'title' => 'Maximum Value',
      'type' => 'textfield',
      'placeholder' => 'Maximum value allowed for the input.',
      'htmlClass' => 'col-xs-4',
    ];

    $fields['form_item_step'] = [
      'title' => 'Step',
      'type' => 'textfield',
      'placeholder' => 'Step attribute to specify the legal number intervals.',
      'htmlClass' => 'col-xs-4',
    ];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function render($element_settings, $element_markup, $element_class) {
    $content = parent::render($element_settings, $element_markup, $element_class);

    if (empty($content)) {
      return $content;
    }

    $content['wrapper'][$element_settings['form_item_name']]['#attributes']['class'][1] = 'ss-form-input-number-field';
    $content['wrapper'][$element_settings['form_item_name']]['#attributes']['type'] = 'number';

    if ($element_settings['form_item_min'] !== '') {
      $content['wrapper'][$element_settings['form_item_name']]['#attributes']['min'] = $element_settings['form_item_min'];
    }

    if ($element_settings['form_item_max'] !== '') {
      $content['wrapper'][$element_settings['form_item_name']]['#attributes']['max'] = $element_settings['form_item_max'];
    }

    if ($element_settings['form_item_step'] !== '') {
      $content['wrapper'][$element_settings['form_item_name']]['#attributes']['step'] = $element_settings['form_item_step'];
    }

    return $content;
  }

}
